function filteredeeg = zerophasefilter(raweeg, filter)
%
%   zerophasefilter(raweeg, filter)
%
% Applies the specified zero-phase finite impulse response FILTER to the 
% the specified RAWEEG structure. Returns a FILTEREDEEG structure whose 
% data field contains the filtered amplitude, instantaneous phase 
% (extracted by Hilbert transform), and envelope magnitude (extracted by 
% Hilbert transform). 
%
% Note that the sampling rate of the FILTER must match the sampling rate 
% of the eeg data. Also, note that FILTER must have a finite impulse 
% response which is symmetric about zero and has an odd number of 
% coefficients. FILTER is a special structure with fields 'descript', 
% 'kernel' and 'samprate'.
%
% Finally, be aware that the very beginning and end time segments of the
% filtered output are not to be trusted because the required samples are
% missing from the input.
%
% written by smk

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ERROR CHECKING

% Check that sampling rates of the filter and the data match to within some
% very small epsilon
if (abs(raweeg.samprate-filter.samprate)>1e-3)
    error('samprate of filter does not match samprate of eegstruct')
end

% Check that the supplied raweeg data is a valid column vector
if isempty(raweeg.data)
    error('eeg data is empty')
elseif length(raweeg.data) ~= numel(raweeg.data)
    error('eeg data must be a column vector')
else
    [numrows, numcols] = size(raweeg.data);
    if numrows==1
        error('eeg data must be a column vector')
    end
end

% Check that the filter kernel is a non-empty vector
if isempty(filter.kernel)
    error('filter kernel is empty')
elseif length(filter.kernel) ~= numel(filter.kernel)
    error('filter kernel must be a vector')
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NOW FOR THE ACTUAL BUSINESS

% copy the entire cell array structure
filteredeeg = raweeg;

% wipe out the corresponding data field and allocate a 3-column matrix
filteredeeg.data = zeros(length(raweeg.data),3);
% filter over the dataset user the Signal Processing toolbox filtfilt function
temp = filtfilt(filter.kernel,1,raweeg.data);
% write this temp array into the first column of output.data
filteredeeg.data(:,1) = temp;
% now compute the Hilbert transform over temp
temp = hilbert(temp);
% write the angle and magnitude to the second and third columns
filteredeeg.data(:,2) = angle(temp);
filteredeeg.data(:,3) = abs(temp);

% overwrite the fields field to reflect the contents of the three columns
filteredeeg.fields = 'filtered_amplitude instantaneous_phase envelope_magnitude';

% finally edit the descript field of this cell to reflect that this is 
% *filtered* data, not the original raw eeg trace
filteredeeg.descript = strvcat(raweeg.descript, ['filtered through' filter.descript]);


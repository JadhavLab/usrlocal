function ripplesnippets = findripples(ripple,threshold,baseline,minimum_duration)
%
%   ripplesnippets = findripples(ripple,threshold,baseline,minimum_duration)
%
% Scan the ripple eeg struct to snip out start and stop time indices 
% for ripple events. Events whose amplitude envelope exceed threshold
% will be snipped out over points which are above baseline (using a 
% watershed algorithm which scans from the threshold-crossings down to 
% baseline). Overlapping snippets will be combined intelligently, and 
% snippets which have duration shorter than minimum_duration will be culled.
%
% threshold and baseline are to be expressed in eeg amplitude units
% minimum_duration is to be expressed in seconds
%
% Returns a ripplesnippets struct with some rather self-explanatory text and 
% scalar fields, and a .data field with indices.
%
% Each cell of ripplesnippets.data is an Nx3 matrix, in which each row 
% corresponds to a ripple event. The first column is the start index of 
% each ripple, the second column is the index of the "middle" of the 
% ripple (useful for arbitrary alignment of ripple events), the third 
% column is the end index.
%
% Written by smk.

% now determine whether the Hilbert envelope of the ripple-filtered trace
% falls above, below, or right on this threshold within each LIA interval
% we keep this information in a cell array called flags, whose organization 
% parallels that of LIA_intervals
flags = sign(ripple.data(:,3) - threshold);
% within each cell of flags (i.e. for each LIA interval), we determine which
% samples fall above or below threshold according to the flags in flags
% if there exist zero entries (i.e. where the envelope exactly meets the threshold), 
% we apply a greedy neighbor iteration to flag them as +1 (above) or -1 (below)
if ~all(flags)
    while true % keep looping until the break condition is met below
	    borderline_samples = find(flags==0); % find the indices to all elements of ripple.data(:,1) that are exactly at threshold
        % discard endpoint entries if they correspond to very first or very last eeg samples
    	if (borderline_samples(1)==1)
	        borderline_samples(1) = [];
		elseif (borderline_samples(end)==length(ripple.data))
		    borderline_samples(end) = [];	
    	end
        % for each zero flag in flags, check the immediately preceding and following 
        % neighbor flags. if either of these is +1 (i.e. above threshold), then we 
        % "greedily" bump up this element to +1 as well (it makes sense if you think about 
        % it carefully)
        % the + sign here at the front of the parentheses serves to convert the 
        % logical values of the boolean equality tests to numerics
        % note that I am using the single pipe | OR operator, not the double pipe ||
        % this is because I want to compute element-by-element OR to return a vector of 
        % results, not a scalar over the whole vector
        flags_to_flip_positive = +((flags(borderline_samples(:)-1)==1)|(flags(borderline_samples(:)+1)==1));
        if any(elements_to_flip_positive)
            flags(borderline_samples(:))=flags_to_flip_positive;
        else
        % we keep doing this until there is no more possibility of change in the elements of flags
            break
        end
   end
   % and once we exit the while block, all remaining zero flags are converted to -1
   flags(flags==0) = -1;
end

% now proceed, having gotten rid of all those pesky zero values
% compute first-order differences. ascending threshold-crossings give diff of +2, 
% whereas descending threshold-crossings give diff of -2
% note that ascending and descending crossings must alternate; this makes it easy 
% to identify each interval
% Also need to be careful about the boundary cases (what happens at the beginning or 
% end of the ripple trace)
threshold_crossings = diff(flags);
ascending_crossings = find(threshold_crossings == +2) + 1; % the +1 is to account for the way diff works
descending_crossings = find(threshold_crossings == -2);
if (descending_crossings(1) < ascending_crossings(1)) % if the trace begins above threshold
    % then remove the first element of descending_crossings 
    % we don't want to grab a truncated ripple at the beginning of the eeg trace
    descending_crossings(1) = [];
elseif (ascending_crossings(end) > descending_crossings(end)) % likewise at the end of the trace
    ascending_crossings(end) = [];
end

% we should end up with equal numbers of ascending and descending crossings
if length(ascending_crossings)~=length(descending_crossings)
    error('There is a bug in the calculation of threshold-crossings!')
end
    
% okay, now that we have the threshold-crossings all collected, we now
% have a basic handle on all ripple events. However, there remain two problems:
% (1) we haven't snipped each ripple event in its entirety - we may be missing 
% the beginning and end which fall below our threshold; and (2) we may be 
% snipping out adjacent portions of the same ripple event. We need to iron out
% these issues and then populate the output array.

% construct a look-up vector of indices where the Hilbert envelope falls below 
% the watershed level (which should be set so that it is somewhere in the "noise")
noise_samples = find(ripple.data(:,3) < baseline); 
% ripples that are too close to the beginning or end of the record 
% may be truncated before they are able to descend into the baseline noise
% we check whether any of the ripple snippets that have been identified 
% are cut off at the endpoints
while true
    if isempty(find(noise_samples > descending_crossings(end)))
        descending_crossings(end) = [];
        ascending_crossings(end) = [];
    elseif isempty(find(noise_samples < ascending_crossings(1)))
        ascending_crossings(1) = [];
        descending_crossings(1) = [];
    else
        break
    end
end

% again, check that we end up with equal numbers of ascending and descending crossings
if length(ascending_crossings)~=length(descending_crossings)
    error('There is a bug in the culling of truncated ripples!')
end
% now we allocate an array to hold the output of indices to delineate ripple events
ripple_indices = zeros(length(ascending_crossings),3);

% now scan before and after each above-treshold ripple top (think of an iceberg)
% to find the nearest sample that is below noise level
for j = 1:size(ripple_indices,1)
    ripple_indices(j,1) = max(noise_samples(noise_samples < ascending_crossings(j))) - 1;
    ripple_indices(j,3) = min(noise_samples(noise_samples > descending_crossings(j))) - 1;
end

% test for overlap and combine/delete as necessary
while true
    can_quit_iterating = true;
    for j = 1:(size(ripple_indices,1)-1)
        if (ripple_indices(j,3) >= ripple_indices(j+1,1))
            can_quit_iterating = false; % if I've found something to change, I can't quit iterating
            ripple_indices(j,3) = ripple_indices(j+1,3);
            ripple_indices(j+1,1) = NaN; % note that comparisons against NaN always give false; 
                                                % this bypasses the test condition on the next go-through
        end
    end
    % now delete the rows correspond to intervals that have already been accounted for in overlap
    [rows,cols] = find(isnan(ripple_indices));
    ripple_indices(rows,:) = [];
    if can_quit_iterating % this is true only if I've scanned the entire dataset without changing anything
        break
    end
end

% also, delete rows corresponding to intervals which are too brief (we don't like fleeting ripples)
while true
    can_quit_iterating = true;
    for j = 1:size(ripple_indices,1)
        if (ripple_indices(j,3)-ripple_indices(j,1) < minimum_duration*ripple.samprate)
            can_quit_iterating = false; % if I've found something to change, I can't quit iterating
            ripple_indices(j,1) = NaN; % note that comparisons against NaN always give false; 
                                                % this bypasses the test condition on the next go-through
        end
    end
    % now delete the rows correspond to intervals that do not satisfy minimum_duration
    [rows,cols] = find(isnan(ripple_indices));
    ripple_indices(rows,:) = [];
    if can_quit_iterating % this is true only if I've scanned the entire dataset without changing anything
        break
    end
end

% finally, compute the "median" (in time) of the energy of each ripple event
for j = 1:size(ripple_indices,1)
    % grab a slice of the ripple trace, square the values, compute cumulative sums
    ripple_energy_cumulatives = cumsum(ripple.data(ripple_indices(j,1):ripple_indices(j,3),1).^2);
    % find the index of the element of ripple_energy_cumulatives which is closest to 
    % 1/2 of the total, and assign this to be the "center" of the ripple event 
    % in the second column of the output array
    [value, index] = min(abs(ripple_energy_cumulatives - ripple_energy_cumulatives(end)/2));
    ripple_indices(j,2) = ripple_indices(j,1) + index - 1;
end

% now package it in a struct
ripplesnippets = struct('descript',strvcat('array indices for start, center, end of ripple events occurring in dataset:',ripple.descript));
ripplesnippets.starttime = ripple.starttime;
ripplesnippets.samprate = ripple.samprate;
ripplesnippets.threshold = threshold;
ripplesnippets.baseline = baseline; 
ripplesnippets.minimum_duration = minimum_duration;
ripplesnippets.indices = ripple_indices;

function ISIviewer(clustnum,handles)

global clustattrib;
global clustdata;

plotfig = figure('Color',get(0,'DefaultUicontrolBackgroundColor'), ... 
        'Tag','viewISIfig','NumberTitle','off','Name',['ISI viewer: Cluster ',num2str(clustnum)]); 
% axis ranges from 1ms to 1000ms
% this is hard-coded; sorry for the bad design
xtickloc = 0:1:3;
a = axes();

index = clustattrib.clusters{clustnum}.index;
% spiketimes in milliseconds
spiketimes = 1e3*double(clustdata.params(index,1)) / ...
  (clustdata.UnitsPerSec);
logISI = log10(diff(spiketimes));
edges = xtickloc(1):0.05:xtickloc(end);
% define an interval which could possibly fall within the 
% refractory period of a neuron; inter-event intervals that are 
% less than CRITERION invite scrutiny
CRITERION = 2; 
counts = histc(logISI,edges);
counts_below = histc(logISI(logISI < log10(CRITERION)),edges);
% stacked histogram
for i = 1:(length(edges)-1)
    patch([edges(i) edges(i+1) edges(i+1) edges(i)], ...
      [0 0 counts_below(i) counts_below(i)],'r');
    patch([edges(i) edges(i+1) edges(i+1) edges(i)], ...
      [counts_below(i) counts_below(i) counts(i) counts(i)],'k');
end
% draw line warning of possible refactory-period violations
ylim = get(a,'YLim');
line('XData',log10(CRITERION)*ones(1,2), ...
  'YData',[ylim(1) ylim(end)], ...
  'Color','r','LineStyle','--','LineWidth',2);
text(log10(CRITERION),ylim(end),sprintf(' %2.2f ms',CRITERION), ...
  'VerticalAlignment','top','HorizontalAlignment','left','Color','r');
set(a,'XLim',[xtickloc(1) xtickloc(end)],'XTick',xtickloc, ... 
  'XTickLabel',arrayfun(@num2str,10.^xtickloc,'UniformOutput',false));
xlabel(a,'inter-event interval (ms)');
ylabel(a,'counts');


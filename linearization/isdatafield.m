% toknum = ISDATAFIELD(fieldstring, fieldname)
%     Checks for fieldname as a token in fieldstring and returns the token
%     number if it exists and zero otherwise

% Ryan -- encountered case where .fields coming out of shantanu's pipeline
% was a cell and not a string so adapted this code so that it could still
% report position for that situation.
function [loc] = isdatafield(fs, fn)


if isstr(fs)
    toknum = 0;
    loc = 0;
    rem = fs;
    while (~isempty(rem))
        [tok rem] = strtok(rem);
        toknum = toknum + 1;
        if (strcmp(tok, fn))
            loc = toknum;
            return;
        end
    end
end

    % RYAN -- added section for case where fs = cell type
if iscell(fs)
    loc = 0;
    for f = 1:numel(fs)
        if isequal(fs{f},fn)
            loc = f;
        end
    end
end


end

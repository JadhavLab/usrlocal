%COUNTS = ROWCOUNT(VALUES, LOOKUPVALUES)
%
%Counts the number of rows in LOOKUPVALUES matches each row in VALUES
%and returns the counts.
%
%